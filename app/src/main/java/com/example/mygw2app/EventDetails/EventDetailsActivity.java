package com.example.mygw2app.EventDetails;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.mygw2app.Event;
import com.example.mygw2app.MainActivity;
import com.example.mygw2app.R;
import com.google.android.material.tabs.TabLayout;

import org.joda.time.LocalTime;

import java.util.ArrayList;

public class EventDetailsActivity extends AppCompatActivity {

    private static final String TAG = "EventDetailsActivity";

    private ArrayList<Event> arrayOfEvents;
    private ArrayList<Event> arrayOfEventBosses;

    private String[] eventNames = new String[] {
            "Kill the Svanir shaman chief to break his control over the ice elemental.",
            "Destroy the fire elemental created from chaotic energy fusing with the C.L.E.A.N. 5000's energy core.",
            "Defeat the shadow behemoth.",
            "Defeat the great jungle wurm.",
            "Defeat Ulgoth the Modniir and his minions.",
            "Kill Admiral Taidha Covington.",
            "Slay the Shatterer",
            "Kill the megadestroyer before it blows everyone up.",
            "Defeat the Inquest's golem Mark II.",
            "Defeat the Claw of Jormag.",
            "Defeat the amber head of the great jungle wurm.",
            "Defeat Tequatl the Sunless.",
            "Defeat the Karka Queen threatening the settlements."
    };

    private String[] bossNames = new String[] {
            "Svanir Shaman Chief",
            "Fire Elemental",
            "Shadow Behemoth",
            "Great Jungle Wurm",
            "Modniir Ulgoth",
            "Admiral Taidha Covington",
            "The Shatterer",
            "Megadestroyer",
            "Inquest Golem Mark II",
            "Claw of Jormag",
            "Evolved Jungle Wurm",
            "Tequatl the Sunless",
            "Karka Queen"
    };

    private Integer[] bossSchedule = new Integer[] {
            2,
            00,
            15,
            2,
            00,
            45,
            2,
            01,
            45,
            2,
            01,
            15,
            3,
            01,
            30,
            3,
            00,
            00,
            3,
            01,
            00,
            3,
            00,
            30,
            3,
            02,
            00,
            3,
            02,
            30,
            0,
            00,
            00,
            0,
            00,
            00,
            0,
            00,
            00
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_list_main);

        arrayOfEvents = MainActivity.arrayOfEvents;
        arrayOfEventBosses = getEventBosses(arrayOfEvents);

        setupViewPager();
    }

    private void setupViewPager(){
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new EventFragment(arrayOfEventBosses));
        adapter.addFragment(new EventFragment(arrayOfEvents));

        ViewPager viewPager = findViewById(R.id.EventListContainer);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText(R.string.tab_title_1);
        tabLayout.getTabAt(1).setText(R.string.tab_title_0);
    }

    private ArrayList<Event> getEventBosses(ArrayList<Event> events) {
        ArrayList<Event> temp = new ArrayList<Event>();

        for (int i = 0; i < eventNames.length; i++) {
            for (int j = 0; j < events.size(); j++) {
                Event event = events.get(j);
                if (event.getName().equals(eventNames[i])) {
                    ArrayList<LocalTime> list = new ArrayList<>();
                    int l = i*3;
                    int factor = bossSchedule[l];
                    int hh = bossSchedule[l+1];
                    int mm = bossSchedule[l+2];

                    if (factor != 0) {
                        while (hh < 24) {
                            list.add(new LocalTime(hh, mm));
                            hh += factor;
                        }
                    }

                    event.setName(bossNames[i]);
                    event.setSchedule(list);

                    if (i > 9) {
                        event.setIcon(R.drawable.event_red_boss);
                    }

                    temp.add(event);
                    break;
                }
            }
        }

        return temp;
    }

}
