package com.example.mygw2app.EventDetails;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mygw2app.Event;
import com.example.mygw2app.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.EventViewHolder> {

    private static final String TAG = "RecycleViewAdapter";

    private Context mContext;
    private ArrayList<Event> events;

    private DateTimeFormatter dateTimeFormatter;
    private DateTimeZone dateTimeZone;
    private DateTime dateTime;

    public RecycleViewAdapter(Context mContext, ArrayList<Event> events) {
        this.mContext = mContext;
        this.events = events;

        dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");
        dateTimeZone = DateTimeZone.forOffsetHours(3);
        dateTime = new DateTime(dateTimeZone);
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, parent, false);
        EventViewHolder holder = new EventViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, final int position) {
        ArrayList<LocalTime> list = events.get(position).getSchedule();

        holder.eImage.setImageResource(events.get(position).getIcon());
        holder.eName.setText(events.get(position).getName());
        holder.eLevel.setText(events.get(position).getLevel().toString());
        holder.eLocation.setText("Location: " + events.get(position).getLocation());
        holder.eNext.setText("Next: ");

        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                DateTime eventTime = list.get(i).toDateTimeToday(dateTimeZone);
                if (dateTime.getMillis() < eventTime.getMillis()) {
                    long diff = dateTime.getMillis() - eventTime.getMillis();
                    int minutes = (int) ((diff / (1000*60)) % 60);
                    int hours   = (int) ((diff / (1000*60*60)) % 24);

                    holder.eNext.setText("Next: " + eventTime.toString(dateTimeFormatter));
                    holder.eIn.setText("in: " + -hours + "h " + -minutes + " m");
                    break;
                }
            }
        }

        holder.eParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, events.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout eParentLayout;
        ImageView eImage;
        TextView eName;
        TextView eLevel;
        TextView eLocation;
        TextView eNext;
        TextView eIn;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);

            eParentLayout = itemView.findViewById(R.id.event_parent_layout);
            eImage = itemView.findViewById(R.id.event_image);
            eName = itemView.findViewById(R.id.event_name);
            eLevel = itemView.findViewById(R.id.event_level);
            eLocation = itemView.findViewById(R.id.event_map);
            eNext = itemView.findViewById(R.id.event_next);
            eIn = itemView.findViewById(R.id.event_in);
        }
    }
}
