package com.example.mygw2app.EventDetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mygw2app.Event;
import com.example.mygw2app.R;

import java.util.ArrayList;

public class EventFragment extends Fragment {

    private static final String TAG = "EventFragment";

    private ArrayList<Event> arrayOfEvents;

    public EventFragment(ArrayList<Event> events) {
        arrayOfEvents = events;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_list_fragment, container, false);

        initRecycleView(view, arrayOfEvents);

        return view;
    }

    private void initRecycleView(View view, ArrayList<Event> items) {
        RecyclerView recyclerView = view.findViewById(R.id.fragment_recycle_view);
        RecycleViewAdapter adapter = new RecycleViewAdapter(getContext(), items);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}