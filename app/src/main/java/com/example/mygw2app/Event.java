package com.example.mygw2app;

import android.util.Log;

import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Event {

    private static final String TAG = "Event";

    private String event_id;
    private String name;
    private Integer level;
    private String location;
    private String flags;
    private ArrayList<LocalTime> schedule;
    private Integer icon;

    public String getEvent_id() {
        return event_id;
    }
    public String getName() {
        return name;
    }
    public Integer getLevel() {
        return level;
    }
    public String getLocation() {
        return location;
    }
    public String getFlags() {
        return flags;
    }
    public ArrayList<LocalTime> getSchedule() {
        return schedule;
    }
    public Integer getIcon() {
        return icon;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setSchedule(ArrayList<LocalTime> schedule) {
        this.schedule = schedule;
    }
    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public Event(JSONObject object, ArrayList<Map> maps) {
        try {
            this.event_id = object.getString("id");
            this.name = object.getString("name");
            this.level = object.getInt("level");
            this.location = getMapName(maps, object.getInt("map_id"));
            this.schedule = new ArrayList<>();
            this.flags = (object.getJSONArray("flags").isNull(0) ? "" : object.getJSONArray("flags").get(0).toString());
            this.icon = (this.flags == "" ? R.mipmap.ic_launcher_round : (this.flags.equals("meta_event") ? R.drawable.event_red_boss : R.drawable.event_boss));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Event> fromJson(JSONArray jsonObjects, ArrayList<Map> maps) {
        ArrayList<Event> events = new ArrayList<Event>();
        JSONObject object;

        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                object = jsonObjects.getJSONObject(i);
                events.add(new Event(object, maps));
            } catch (JSONException e) {
                Log.e(TAG, "fromJson: " + e);
                e.printStackTrace();
            }
        }

        return events;
    }

    private static String getMapName(ArrayList<Map> maps, Integer id) {
        String mapName = "";
        for (int i = 0; i < maps.size(); i++) {
            Map map = maps.get(i);
            if (map.getMap_id().equals(id)) {
                mapName = map.getMap_name();
                break;
            }
        }
        return mapName;
    }
}