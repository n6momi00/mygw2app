package com.example.mygw2app;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.util.Log;
import android.view.MenuItem;

import com.example.mygw2app.EventDetails.EventDetailsActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    private JSONArray jsonArray;                            // Array from json
    private ArrayList<Map> arrayOfMaps;                     // Array of Maps
    public static ArrayList<Event> arrayOfEvents;           // Array of Events
    private String mapsFileName = "myJsonMaps";
    private String eventsFileName = "myJsonEvents";
    private String urlEventDetails = "https://api.guildwars2.com/v1/event_details.json";
    private String urlMaps = "https://api.guildwars2.com/v1/maps.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        setSupportActionBar(toolbar);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            intent = new Intent(this, EventDetailsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        run();
    }

    private void run() {
        if (fileExist(mapsFileName) && fileExist(eventsFileName)) {
            try {
                JSONArray mapsTemp = new JSONArray(readFromFile(mapsFileName));
                jsonArray = new JSONArray(readFromFile(eventsFileName));
                arrayOfMaps = Map.fromJson(mapsTemp);
                arrayOfEvents = Event.fromJson(jsonArray, arrayOfMaps);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            new GetJsonData().execute(urlEventDetails, urlMaps);
        }
    }

    private boolean fileExist(String fname) {
        File file = getBaseContext().getFileStreamPath(fname);
        Log.i(TAG, "fileExist: " + file.exists());
        return file.exists();
    }

    private void writeToFile(JSONArray data, String fname) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(fname, Context.MODE_PRIVATE));
            outputStreamWriter.write(data.toString());
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
    }

    private String readFromFile(String fname) {
        String ret = "";

        try {
            InputStream inputStream = openFileInput(fname);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e(TAG, "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e(TAG, "Can not read file: " + e.toString());
        }

        return ret;
    }

    private class GetJsonData extends AsyncTask<String, Void, GetJsonData.Wrapper> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(TAG, "Json Data is downloading...");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this,"Json Data is downloading...", Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        protected Wrapper doInBackground(String... param) {
            Wrapper wrapper = new Wrapper();
            wrapper.result1 = Get(param[0], "events");
            wrapper.result2 = Get(param[1], "maps");
            return wrapper;
        }

        @Override
        protected void onPostExecute(Wrapper wrapper) {
            //super.onPostExecute(wrapper);
            writeToFile(wrapper.result1, eventsFileName);
            writeToFile(wrapper.result2, mapsFileName);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this,"Done!", Toast.LENGTH_LONG).show();
                }
            });
        }

        public class Wrapper {
            public JSONArray result1;
            public JSONArray result2;
        }
    }

    public static synchronized JSONArray Get(String url, String objName) {

        HttpHandler httpHandler = new HttpHandler();
        String jsonString = httpHandler.makeServiceCall(url);

        if (jsonString != null) {
            try {
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONObject objects = jsonObject.getJSONObject(objName);
                Iterator eventsObjKey = objects.keys();

                while (eventsObjKey.hasNext()) {
                    String eventKey = eventsObjKey.next().toString();
                    JSONObject eventValue = objects.getJSONObject(eventKey);
                    eventValue.put("id", eventKey);
                    jsonArray.put(eventValue);
                }
                return jsonArray;

            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");
        }
        return null;
    }

}