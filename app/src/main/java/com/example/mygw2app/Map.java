package com.example.mygw2app;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Map {

    private static final String TAG = "Map";

    private Integer map_id;
    private String map_name;
    private Integer min_level;
    private Integer max_level;
    private Integer default_floor;
    private JSONArray floor;
    private Integer region_id;
    private String region_name;
    private Integer continent_id;
    private String continent_name;
    private JSONArray map_rect;
    private JSONArray continent_rect;

    public Integer getMap_id() {
        return map_id;
    }
    public String getMap_name() {
        return map_name;
    }
    public Integer getMin_level() {
        return min_level;
    }
    public Integer getMax_level() {
        return max_level;
    }
    public Integer getDefault_floor() {
        return default_floor;
    }
    public JSONArray getFloor() {
        return floor;
    }
    public Integer getRegion_id() {
        return region_id;
    }
    public String getRegion_name() {
        return region_name;
    }
    public Integer getContinent_id() {
        return continent_id;
    }
    public String getContinent_name() {
        return continent_name;
    }
    public JSONArray getMap_rect() {
        return map_rect;
    }
    public JSONArray getContinent_rect() {
        return continent_rect;
    }

    public Map(JSONObject object) {
        try {
            this.map_id = object.getInt("id");
            this.map_name = object.getString("map_name");
            this.min_level = object.getInt("min_level");
            this.max_level = object.getInt("max_level");
            this.default_floor = object.getInt("default_floor");
            this.floor = object.getJSONArray("floor");
            this.region_id = object.getInt("region_id");
            this.region_name = object.getString("region_name");
            this.continent_id = object.getInt("continent_id");
            this.continent_name = object.getString("continent_name");
            this.map_rect = object.getJSONArray("map_rect");
            this.continent_rect = object.getJSONArray("continent_rect");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Map> fromJson(JSONArray jsonObjects) {
        ArrayList<Map> maps = new ArrayList<Map>();
        JSONObject object;

        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                object = jsonObjects.getJSONObject(i);
                maps.add(new Map(object));

            } catch (JSONException e) {
                Log.e(TAG, "fromJson: " + e);
                e.printStackTrace();
            }
        }

        return maps;
    }

}